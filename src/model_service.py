import logging
import os
import numpy as np
import pandas as pd
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from sklearn.preprocessing import StandardScaler
import joblib

logger = logging.getLogger(__name__)

FILENAME_MODEL = os.path.join("model", "gradient_boosting_model.pkl")
FILENAME_SCALER = os.path.join("model", "scaler.pkl")
FILENAME_CSV = os.path.join("csv", "orcamentos.csv")


REQUIRED_FIELDS_PREDICT = {
    "e_valor_hora": float | int,
    "e_tmp_servico": float | int,
    "e_margem_material": float | int,
    "e_complexidade_1_3": float | int,
    "s_valor_almoco": float | int,
    "s_distancia_do_servico_km": float | int,
    "s_preco_combustivel": float | int,
    "s_custo_manut_autom": float | int,
    "s_custo_imposto": float | int,
    "s_custo_ferram": float | int,
    "s_tmp_servico_func": float | int,
    "s_valor_hora_func": float | int,
    "s_qt_func": float | int,
    "s_valor_materia": float | int,
}

REQUIRED_FIELDS = {
    **REQUIRED_FIELDS_PREDICT,
    "orcamento": float | int,
}


class ModeloService:

    @classmethod
    def add_instance(cls, data: dict) -> None:
        cls._validate(data, REQUIRED_FIELDS)

        try:
            df_new = pd.DataFrame({key: [value] for key, value in data.items()})
            existing_df = pd.read_csv(FILENAME_CSV)

            # Remover linhas duplicadas, considerando todas as colunas exceto a última
            existing_df.drop_duplicates(
                existing_df.columns[:-1], keep="first", inplace=True
            )

            # Índices repetidos
            repeated_indices = np.repeat(existing_df.index, 10)
            # Novo DataFrame com linhas repetidas
            existing_df = existing_df.reindex(repeated_indices).reset_index(drop=True)

            mask = (
                existing_df.drop(columns=["orcamento"])
                == df_new.drop(columns=["orcamento"]).iloc[0]
            ).all(axis=1)

            if mask.any():
                existing_df = existing_df[~mask]

            df_new = pd.DataFrame({key: [value] * 10 for key, value in data.items()})
            df = pd.concat([existing_df, df_new], ignore_index=True)

            df = df.round(2)
            df = df.sort_values(by="orcamento")
            df.to_csv(FILENAME_CSV, index=False)

        except FileNotFoundError:
            pass

        cls.update_model()

    @classmethod
    def test_model_fixo(cls) -> float:
        model = joblib.load(FILENAME_MODEL)
        scaler = joblib.load(FILENAME_SCALER)

        nova_instancia = pd.DataFrame(
            {
                "e_valor_hora": [45.0],
                "e_tmp_servico": [1.0],
                "e_margem_material": [0.1],
                "e_complexidade_1_3": [0.0],
                "s_valor_almoco": [20.0],
                "s_distancia_do_servico_km": [1.0],
                "s_preco_combustivel": [5.89],
                "s_custo_manut_autom": [0.03],
                "s_custo_imposto": [0.27],
                "s_custo_ferram": [0.01],
                "s_tmp_servico_func": [0.0],
                "s_valor_hora_func": [0.0],
                "s_qt_func": [0.0],
                "s_valor_materia": [10.0],
            }
        )

        nova_instancia_scaled = scaler.transform(nova_instancia)

        # Fazer a previsão
        previsao = model.predict(nova_instancia_scaled)
        return float(previsao[0])

    @classmethod
    def update_model(cls) -> None:
        # Carregar os dados do CSV
        df = pd.read_csv(FILENAME_CSV)

        # Separar features do target/label
        X = df.drop(columns=["orcamento"], axis=1)
        y = df["orcamento"]

        # Dividir os dados em conjuntos de treinamento e teste
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.2, random_state=42
        )

        # Normalizar os dados
        scaler = StandardScaler()
        X_train_scaled = scaler.fit_transform(X_train)
        X_test_scaled = scaler.transform(X_test)

        # Criar um modelo usando Gradient Boosting
        model = GradientBoostingRegressor(
            learning_rate=0.1, max_depth=5, n_estimators=200, random_state=42
        )
        model.fit(X_train_scaled, y_train)
        y_pred = model.predict(X_test_scaled)
        r2 = r2_score(y_test, y_pred)
        print(f"Gradient Boosting R^2: {r2}")
        if r2 > 0.95:
            # Salvar o modelo e o scaler
            cls._ensure_directory_exists("model")
            joblib.dump(model, FILENAME_MODEL)
            joblib.dump(scaler, FILENAME_SCALER)
        else:
            raise Exception("Very low score")

    @classmethod
    def predict(cls, data: dict) -> dict:
        cls._validate(data, REQUIRED_FIELDS_PREDICT)
        model = joblib.load(FILENAME_MODEL)
        scaler = joblib.load(FILENAME_SCALER)
        nova_instancia = pd.DataFrame({key: [value] for key, value in data.items()})
        nova_instancia_scaled = scaler.transform(nova_instancia)
        previsao = model.predict(nova_instancia_scaled)
        return {"label": float(previsao[0])}

    @classmethod
    def _ensure_directory_exists(cls, directory):
        if not os.path.exists(directory):
            os.makedirs(directory)

    @classmethod
    def _validate(cls, data: dict, required_fields: dict) -> None:
        for field, field_type in required_fields.items():
            if field not in data:
                raise ValueError(f"The required field '{field}' is missing.")
            if not isinstance(data[field], field_type):
                raise TypeError(
                    f"The field '{field}' must be of type {field_type.__name__}."
                )
