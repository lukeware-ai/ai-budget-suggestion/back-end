import multiprocessing

bind = "0.0.0.0:8884"
workers = multiprocessing.cpu_count() * 2 + 1
loglevel = "debug"
accesslog = "-"  # "-" significa stdout
errorlog = "-"  # "-" significa stderr