from http import HTTPStatus
import logging
import os
from flask import Flask, jsonify
from mode_controller import model_controller
from flask_cors import CORS

# Configuração de logging para mostrar apenas mensagens CRITICAL
format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=format)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(format)
handler.setFormatter(formatter)
logger.addHandler(handler)

app = Flask(__name__)
CORS(app)

VERSION_API = "api/v1"
app.register_blueprint(model_controller, url_prefix=f"/calc-budget/{VERSION_API}/")

if __name__ != "__main__":
    gunicorn_logger = logging.getLogger("gunicorn.error")
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(logging.CRITICAL)  # Configura o nível de logging para CRITICAL

@app.errorhandler(ValueError)
def handle_value_error(e):
    app.logger.error(f"ValueError: {str(e)}")
    return jsonify({"error": str(e)}), HTTPStatus.BAD_REQUEST

@app.errorhandler(Exception)
def handle_exception_error(e):
    app.logger.error(f"Exception: {str(e)}")
    return jsonify({"error": str(e)}), HTTPStatus.BAD_REQUEST

# Execute o aplicativo Flask
if __name__ == "__main__":
    port = int(os.environ.get("PORT", 8884))
    app.run(host='0.0.0.0', port=port, debug=True)
