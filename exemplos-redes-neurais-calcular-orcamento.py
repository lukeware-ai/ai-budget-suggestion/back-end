import numpy as np
import pandas as pd
import plotly.express as px

from sklearn.metrics import r2_score
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures, StandardScaler, LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingRegressor, RandomForestRegressor

# Carregar os dados do CSV
df = pd.read_csv("csv/orcamentos.csv")

X = df.drop(
    columns=[
        "orcamento",
        "e_lucro_material",
        "s_custo_almoco",
        "s_km_por_l",
        "s_custo_combustivel",
        "s_valor_manut_autom",
        "s_valor_imposto",
        "s_valor_ferram",
        "s_total_func",
        "valor_liquido",
    ],
    axis=1,
)
y = df["orcamento"]

# Dividir os dados em conjuntos de treinamento e teste
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42
)

# Normalizar os dados
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)


# Modelo de RandomForestRegressor
# Random Forest R^2: 0.9945232341749659
# model = RandomForestRegressor(n_estimators=50, random_state=42)
# model.fit(X_train_scaled, y_train)
# y_pred_rf = model.predict(X_test_scaled)
# r2_rf = r2_score(y_test, y_pred_rf)
# print(f'Random Forest R^2: {r2_rf}')

# Modelo de GradientBoostingRegressor
# Gradient Boosting R^2: 0.9951088361926237
model = GradientBoostingRegressor(
    n_estimators=200, learning_rate=0.15, max_depth=4, random_state=42
)
model.fit(X_train_scaled, y_train)
y_pred = model.predict(X_test_scaled)
r2 = r2_score(y_test, y_pred)
print(f"Gradient Boosting R^2: {r2}")

# Modelo de Regressão Linear Basico
# Linear Regression R^2: 0.9868798004580779
# model = LinearRegression()
# model.fit(X_train_scaled, y_train)
# y_pred = model.predict(X_test_scaled)
# r2 = r2_score(y_test, y_pred)
# print(f"Linear Regression R^2: {r2}")


# Pipeline de Regressão Polinomial
# Polynomial Regression R^2: 0.9868798004580779
# model = Pipeline([
#     ('poly', PolynomialFeatures(degree=1)),
#     ('linear', LinearRegression())
# ])
# model.fit(X_train_scaled, y_train)
# y_pred = model.predict(X_test_scaled)
# r2 = r2_score(y_test, y_pred)
# print(f'Polynomial Regression R^2: {r2}')

# Modelo de Ridge Regression
# Ridge Regression R^2: 0.9868798071608805
# model = Ridge(alpha=0.001)
# model.fit(X_train_scaled, y_train)
# y_pred = model.predict(X_test_scaled)
# r2 = r2_score(y_test, y_pred)
# print(f'Ridge Regression R^2: {r2}')

# Modelo de Lasso Regression
# Lasso Regression R^2: 0.9868798137014637
# model = Lasso(alpha=0.1)
# model.fit(X_train_scaled, y_train)
# y_pred = model.predict(X_test_scaled)
# r2 = r2_score(y_test, y_pred)
# print(f'Lasso Regression R^2: {r2}')


# Inicializar e treinar o classificador MLP
# model = MLPClassifier(
#     activation="relu",
#     solver="adam",
#     random_state=42,
#     alpha=0.01,
#     max_iter=150,
#     learning_rate_init=0.01
# )
# model.fit(X_train_scaled, y_train)
# y_pred = model.predict(X_test_scaled)
# r2 = r2_score(y_test, y_pred)
# print(f'Redes Neurais R^2: {r2}')

# Testar nova instância
nova_instancia = pd.DataFrame(
    {
        "e_valor_hora": [45.0],
        "e_tmp_servico": [1.0],
        "e_margem_material": [0.1],
        "e_complexidade_1_3": [0.0],
        "s_valor_almoco": [20.0],
        "s_distancia_do_servico_km": [1.0],
        "s_preco_combustivel": [5.89],
        "s_custo_manut_autom": [0.03],
        "s_custo_imposto": [0.27],
        "s_custo_ferram": [0.01],
        "s_tmp_servico_func": [0.0],
        "s_valor_hora_func": [0.0],
        "s_qt_func": [0.0],
        "s_valor_materia": [10.0],
    }
)

nova_instancia_scaled = scaler.transform(nova_instancia)

# # Fazer a previsão
previsao = model.predict(nova_instancia_scaled)
print(f"A previsão para a nova instância é: {previsao[0]}")


# Função para criar o gráfico de dispersão com Plotly
def plot_real_vs_pred(y_test, y_pred, title):
    fig = px.scatter(
        x=y_test,
        y=y_pred,
        labels={"x": "Valores Reais", "y": "Valores Preditos"},
        title=title,
    )
    fig.add_shape(
        type="line",
        x0=min(y_test),
        y0=min(y_test),
        x1=max(y_test),
        y1=max(y_test),
        line=dict(color="Red", dash="dash"),
    )
    fig.show()


# Criar o gráfico para o modelo de Regressão Linear
# plot_real_vs_pred(y_test, y_pred, 'Regressão Linear: Valores Reais vs. Valores Preditos')
