# Use uma imagem base leve do Python
FROM python:3.12.3-bullseye

# Define o diretório de trabalho dentro do contêiner
WORKDIR /usr/src/app

# Copie os arquivos de requisitos e instale as dependências
COPY requirements.txt ./

# Instale as dependências antes de copiar o código para aproveitar o cache do Docker
RUN pip install --no-cache-dir -r requirements.txt

# Copie o código fonte da aplicação
COPY csv/ ./csv
COPY model/ ./model
COPY src/ .
COPY gunicorn_config.py .

# Lista os arquivos copiados para debug
RUN ls -l /usr/src/app

# Exponha a porta configurada
EXPOSE 8884

# Execute o aplicativo quando o contêiner for iniciado usando Gunicorn
CMD ["sh", "-c", "gunicorn -w 4 -b 0.0.0.0:8884 --config gunicorn_config.py --log-level=debug app:app"]
